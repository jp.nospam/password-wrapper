#NoTrayIcon
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=C:\Users\owner\bin\Steam\steamapps\common\SteamVR\tools\steamvr_environments\game\core\tools\images\vconsole\appicon.ico
#AutoIt3Wrapper_Res_Comment=Password Wrapper
#AutoIt3Wrapper_Res_Description=Password Wrapper
#AutoIt3Wrapper_Res_ProductName=Password Wrapper
#AutoIt3Wrapper_Res_Language=1033
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#include <WinAPIFiles.au3>
#include <GUIListBox.au3>

#region ---Head--
#include <Constants.au3>
#include <GUIConstantsEx.au3>
#include <Misc.au3>
#include <WindowsConstants.au3>

#include <ListBoxConstants.au3>
#include <ButtonConstants.au3>

Global $hGUI,$site_list,$user_list,$password_btn,$site,$user,$password,$gen_password
HotKeySet("{F4}", "typePassword")
create_form()
$password = InputBox("Password?", "(Optional) Provide your master password to automatically generate passwords.")
#endregion ---Head---

#region --- Form ---

func create_form()
$hGUI=GuiCreate("Password Wrapper", 313, 258, -1, -1, $WS_OVERLAPPED + $WS_CAPTION + $WS_SYSMENU + $WS_VISIBLE + $WS_CLIPSIBLINGS + $WS_MINIMIZEBOX)

$site_list = GuiCtrlCreateList("", 10, 10, 130, 201)
$user_list = GuiCtrlCreateList("", 150, 10, 150, 201)
$password_btn = GuiCtrlCreateButton("Password", 90, 220, 100, 30,-1)

GuiSetState()
endfunc
#EndRegion --- Form ---

updateSiteList($site_list)

#region --- Loop ---
While 1
        $msg = GuiGetMsg()
        Select
        Case $msg = $GUI_EVENT_CLOSE
                ExitLoop

        Case $msg = $password_btn
                runPassword()

        Case $msg = $site_list
                        updateUserList()
        Case Else
                ;;;
        EndSelect
WEnd
#Endregion --- Loop ---

func runPassword()
        $site = GuiCtrlRead($site_list)
        $user = GUICtrlRead($user_list)
        $pid = NULL
        if $password <> "" then
                $pid = run("password.exe " & $site & " " & $user & " " & $password,"", @SW_HIDE, $STDOUT_CHILD)
                ProcessWaitClose($pid)
                $gen_password =StdoutRead($pid)
                return
        endif
        $temp_password = InputBox("Password?", "Provide your master password to generate the password.")
		if $password == "" then return
        $pid = run("password.exe " & $site & " " & $user & " " & $temp_password,"", @SW_HIDE, $STDOUT_CHILD)
        ProcessWaitClose($pid)
        $gen_password = StdoutRead($pid)
endfunc

func typePassword()
        send($gen_password, 1)
        $gen_password = ""
endfunc

func updateUserList()
        GUICtrlSetData($user_list, "")
        Local $ini_location = "index.ini"
    $accounts = inireadsection($ini_location, GuiCtrlRead($site_list))
    if IsArray($accounts) then
        for $i = 1 to $accounts[0][0]
            _GUICtrlListBox_AddString($user_list, $accounts[$i][1])
        next
    endif
endfunc

func updateSiteList($list)
    GUICtrlSetData($list, "")
        Local $ini_location = "index.ini"
        Local $ini_data = IniReadSectionNames($ini_location)
    if IsArray($ini_data) then
        for $i = 1 to $ini_data[0]
            _GUICtrlListBox_AddString($list, $ini_data[$i])
        next
    endif
endfunc

#Region --- Additional Functions ---
Func PopulateList()

        Local $ini_location = "index.ini"
        Local $ini_data = IniReadSectionNames($ini_location)

    for $x = 1 to $ini_data[0]
                GuiCtrlSetData(GUICtrlRead($site_list) & $site_list, $ini_data[0])
        Next

EndFunc
#Endregion --- Additional Functions ---

Exit
