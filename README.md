# Password Wrapper

A GUI for password CLI at https://gitlab.com/jp.nospam/password

Press F4 to paste your password. After the password is typed, it is cleared to prevent accidental pasting later.
